
import { sync as globSync } from 'glob'
import { resolve } from 'path'

describe('generate rendered SVGs', function () {
  this.timeout(1000 * 20)

  it('should generate exports containing svgs as strings', async function () {
    const instance = new SVGRenderESM(TEST_FILE)
    await instance.init()

    const exported = await loadModule(TEST_FILE)

    expect(Object.keys(exported)).to.have.lengthOf(
      globSync(resolve(__dirname, '{,!(node_modules)/**/}*svg')).length,
      'does not match same amount found target files')

    for (const item in exported) {
      expect(exported[item]).to.include.string('svg', `${item} is not an <svg/>!`)
    }
  })
  it('should generate one file when "html" glob type is used', async function () {
    const instance = new SVGRenderESM(TEST_FILE, {
      glob: '{,!(node_modules)/**/}*icon*'
    })
    await instance.init()

    const exported = await loadModule(TEST_FILE)

    expect(Object.keys(exported)).to.have.lengthOf(
      globSync(resolve(__dirname, '{,!(node_modules)/**/}*icon*')).length,
      'does not match same amount found target files')

    for (const item in exported) {
      expect(exported[item]).to.include.string('svg', `${item} is not an <svg/>!`)
    }
  })
})

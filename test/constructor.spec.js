
import { name } from '../package.json'

describe(`class ${name} generating new file`, function () {
  this.timeout(1000 * 20)

  it('should create an ECMAScript 6 module file matching name as first argument', async function () {
    let stats
    try {
      stats = statSync(TEST_FILE)
    } catch (err) {
      stats = err
    }

    expect(stats).to.be.a('error', 'control test. file already exists!')

    const instance = new SVGRenderESM(TEST_FILE)

    await instance.init()

    try {
      stats = statSync(TEST_FILE)
    } catch (err) {
      stats = err
    }

    expect(stats).to.be.a('object', 'file was expected to be created.')
  })
})

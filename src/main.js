
import RenderESM from 'render-esm'

import {
  // plugins,
  executeSVGO
} from './execute-svgo.js'

class SVGRenderESM {
  constructor (renderedModuleFile, {
    svgo = {},
    glob = null
  } = {}) {
    // try {
    const config = {
      render: function (path) {
        const options = svgo
        return executeSVGO(path, options)
      } // execute SVGO
    }

    if (glob) {
      config.glob = glob
    } else {
      config.extension = 'svg'
    }

    this._renderESM = new RenderESM(renderedModuleFile,
      {
        header: `SVGo ${(new Date()).toUTCString()}`
      },
      config
    )
    // } catch (err) {
    //   throw err
    // }

    this._svgoOptions = svgo
  }

  async init () {
    await this._renderESM.init()
    this._renderESM.formate()
  }
}

// export plugins
export default SVGRenderESM

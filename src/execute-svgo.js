
import {
  readFileSync
} from 'fs'

import SVGO from 'svgo'
import defaultSettings from './svgo-default-settings.js'

export { defaultSettings as plugins }

/**
 * Execute SVGO on file.
 * @param  {string} path        Path to SVG file.
 * @param  {Array}  settings    Plugin options
 *                              @see  {@link} https://github.com/svg/svgo/tree/master/plugins
 * @return {Object}            `{data, info, path}`
 */
export const executeSVGO = async (path, settings = []) => {
  /** @type {Object[]} Not going to bother merging, just replace everything */
  const plugins = settings.length
    ? settings
    : defaultSettings

  const svgo = new SVGO({ plugins })

  const fileData = readFileSync(path, 'utf8')
  const { data } = await svgo.optimize(fileData, { path })
  return data
}
